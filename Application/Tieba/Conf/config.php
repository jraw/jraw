<?php
return array(
	//'配置项'=>'配置值'
		'siteurl'=>'http://jraw.text.scanlation.info/',
		'cacheurl'=>'http://jraw.text.scanlation.info/public/',
		'siteversion'=>'0.0.2(Alpha)',
		'titleDesc'=>'当生肉加油遇上干柴烈火，造就出了最美味的漫画饕餮盛宴！',
		'metaDesc'=>'当生肉加油遇上干柴烈火，造就出了最美味的漫画饕餮盛宴！——摘自《舌尖上的斯肯雷逊》',
		'metaTag'=>'日本漫画,漫画生肉,漫画,生肉,RAW,Japanese Comic Magzine, Japanese Raw Comic,Manga,Comic',
		'cacheLimit' =>5,
		'pageLimit' =>10,
		
		'cutXmlStart'=>"<div class=\"left_section\" >",
		'cutXmlEnd'=>"<div class=\"right_section right_bright\">",
		'baiduPostUrl'=>'http://tieba.baidu.com/p/',
		'thumbPicUrl'=>'http://imgsrc.baidu.com/forum/abpic/item/',
		'fullPicUrl'=>'http://imgsrc.baidu.com/forum/pic/item/',
		'tiebaRegExp'=>array("/<([bh]r(|\s[^\/>]*))>/","/(\"|')(\S[^>\"']*?=)(\"|')/","/&(\w[^;]*?)=/","/<(img\s*.[^<]*?[^\/])>/","/&nbsp;/"),
		'tiebaRegRep'=>array("<\\1/>","\\1 \\2\\3","&amp;\\1=","<\\1/>"," "),
		
		'duoshuoKey'=>'jraw',
		'duoshuoSecret'=>'7046263a106fcc6a8deced261a73f985',
		'baiduTKey'=>'374e217344391ff9bbdf0a950a57bf6d'
		
		
);