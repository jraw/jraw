<?php
return array(
	//'配置项'=>'配置值'
		'siteurl'=>'http://jraw.text.scanlation.info/',
		'cacheurl'=>'http://jraw.text.scanlation.info/public/',
		'siteversion'=>'0.0.2(Alpha)',
		'cutXmlStart'=>"<div class=\"left_section\" >",
		'cutXmlEnd'=>"<div class=\"right_section right_bright\">",
		'baiduPostUrl'=>'http://tieba.baidu.com/p/',
		'thumbPicUrl'=>'http://imgsrc.baidu.com/forum/abpic/item/',
		'fullPicUrl'=>'http://imgsrc.baidu.com/forum/pic/item/',
		'duoshuoKey'=>'*',
		'duoshuoSecret'=>'*',
		'tiebaRegExp'=>array("/<([bh]r(|\s[^\/>]*))>/","/(\"|')(\S[^>\"']*?=)(\"|')/","/&(\w[^;]*?)=/","/<(img\s*.[^<]*?[^\/])>/","/&nbsp;/"),
		'tiebaRegRep'=>array("<\\1/>","\\1 \\2\\3","&amp;\\1=","<\\1/>"," "),
		'cacheLimit' =>5,
		'pageLimit' =>10
);