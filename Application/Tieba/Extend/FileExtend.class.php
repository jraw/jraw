<?php
namespace Tieba\Extend;

class FileExtend
{
	public function savePostXML($id,$text)
	{
		$date = date('YmdHis');
		
		$fileName = "./cache/post/{$id}-{$date}.xml";
		return $this->saveFile($fileName, $text);
	}
	
	public function savePostImage($id,$text)
	{
		$date = date('YmdH');
	
		$fileName = "./cache/img-{$date}/{$id}.jpg";
		return $this->saveFile($fileName, $text);
	}
	
	public function savePostThumb($id,$text)
	{
	
		$fileName = "./cache/thumbimg/{$id}.jpg";
		return $this->saveFile($fileName, $text);
	}
	
	public function checkThumbExist($id)
	{
		$fileName = "./cache/thumbimg/{$id}.jpg";
		if(file_exists($fileName))
		{
			return $fileName;
		}
		return false;
	}
	
	/**
	 * 保存文件
	 *
	 * @param string $fileName 文件名（含相对路径）
	 * @param string $text 文件内容
	 * @return boolean
	 */
	private function saveFile($fileName, $text)
	{
		
		if (!$fileName || !$text)
			return false;
	
		if ($this->makeDir(dirname($fileName))) {
			if ($fp = fopen($fileName, "w")) {
				if (@fwrite($fp, $text)) {
					fclose($fp);
					return $fileName;
				} else {
					fclose($fp);
					return false;
				}
			}
		}
		return false;
	}
	
	/**
	 * 连续创建目录
	 *
	 * @param string $dir 目录字符串
	 * @param int $mode 权限数字
	 * @return boolean
	 */
	
	private function makeDir($dir, $mode = "0777")
	{
	
		if (!dir) return false;
		if(!file_exists($dir))
		{
			return mkdir($dir,$mode,true);
		} else {
			return true;
		}
	}
}