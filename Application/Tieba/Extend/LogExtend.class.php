<?php
namespace Tieba\Extend;

class LogExtend
{
	private function getUid()
	{
		$data = $_SESSION['userinfo'];
		if(isset($data))
		{
			return $data['id'];
		}else{
			return null;
		}
	}
	
	public function error($desc,$info)
	{
		$this->log($desc, $info, 99);
	}
	
	public function fatal($desc,$info)
	{
		$this->log($desc, $info, 999);
	}
	
	public function debug($desc,$info)
	{
		$this->log($desc, $info, -1);
	}
	
	public function log($desc,$info,$type=0)
	{
		// 0=访问日志
		// 1=登录日志
		// -1=调试日志
		// 99=错误日志
		// 
		$log = M('Log');
		$data = array(
				'type' => $type,
				'uid' => $this->getUid(),
				'desc' => $desc,
				'log' => $info
		);
		$log->data($data)->add();
	}
}