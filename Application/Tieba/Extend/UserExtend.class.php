<?php
namespace Tieba\Extend;

class UserExtend
{
	public function login($data)
	{
		$log = A('Log','Extend');
		if(!is_null($data))
		{
			$user = M("User");
			$user->where('id='.$data['id'])->setField('lastvisittime',date('Y-m-d H:i:s'));
			session('userinfo',$data);
			$ajax = array(
					'status'=>200,
					'desc'=>'登录成功！',
					'name'=>$data['username']
			);
			$log->log('用户'.$data['username'].'(id='.$data['id'].')'.'登录成功', var_export($_SERVER,true),1);
			return $ajax;
		}else{
			$ajax = array(
					'status'=>404,
					'desc'=>'无效的用户数据！'
			);
			return $ajax;
		}
	}
	
	private function httpRequest( $url, $param, $method='POST')
	{
		$header =array();
		$opts = array(
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_SSL_VERIFYHOST => false,
				CURLOPT_HTTPHEADER     => $header
		);
	
		switch(strtoupper($method))
		{
			case 'POST':
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_POST] = 1;
				$opts[CURLOPT_POSTFIELDS] = http_build_query($param);
				break;
			case 'GET':
	
				$opts[CURLOPT_URL] = $url . (stripos('?',$url)?'&':'?') . http_build_query($param);
				break;
			default:
				throw new \Exception('method_not_support');
		}
	
		/* 初始化并执行curl请求 */
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		$data  = curl_exec($ch);
		$error = curl_error($ch);
		curl_close($ch);
		if($error) throw new \Exception('http_error' . $error);
		return  $data;
	}
	
	public function getUserInfo($uid)
	{
		$profile_url = 'http://api.duoshuo.com/users/profile.json';
		$params = array('user_id'=>$uid);
		$response = $this->httpRequest($profile_url, $params,'GET');
		$info = json_decode($response, true);
		//dump($token);
		if ( !is_array($info) ) {
			throw new \Exception("get user profile failed.");
		}
		if(0 == $info['code'])
		{
			return $info['response'];
		}else{
			return false;
		}
	}
	
	//{"access_token":"71308cc0cf87eb99385c0578f0cc7ba7","expires_in":7776000,"user_id":"10441657","remind_in":7775866,"code":0}
	
	/**
	 *
	 * @param string $type
	 * @param array $keys
	 */
	public function getAccessToken( $type, $keys ) {
		$params = array(
				'client_id'	=>	C('duoshuoKey'),
				'client_secret' => C('duoshuoSecret'),
		);
		$end_point = 'http://api.duoshuo.com/';
	
		switch($type){
			case 'token':
				$params['grant_type'] = 'refresh_token';
				$params['refresh_token'] = $keys['refresh_token'];
				break;
			case 'code':
				$params['grant_type'] = 'authorization_code';
				$params['code'] = $keys['code'];
				$params['redirect_uri'] = $keys['redirect_uri'];
				break;
			case 'password':
				$params['grant_type'] = 'password';
				$params['username'] = $keys['username'];
				$params['password'] = $keys['password'];
				break;
			default:
				throw new \Exception("wrong auth type");
		}
		$accessTokenUrl = $end_point . 'oauth2/access_token';
		$response = $this->httpRequest($accessTokenUrl, $params);
	
		$token = json_decode($response, true);
		//dump($token);
		if ( !is_array($token) ) {
			throw new \Exception("get access token failed." . $token['error']);
		}
	
		return $token;
	}
}