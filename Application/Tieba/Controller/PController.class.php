<?php
namespace Tieba\Controller;
use Think\Controller;
class PController extends Controller {
	public function _empty($id){
		$this->showpage($id);
	}
	
	public function index()
	{
		$this->redirect('/Post/index');
	}
	
	protected function showpage($id){
		//echo 'id='.$id;
		$userinfo = $_SESSION['userinfo'];
		$post = M('Post');
		$data = $post->where("postid='{$id}'")->find();
		$log = A('Log','Extend');
		$reffer = $_SERVER['HTTP_REFERER'];
		$log->log('用户请求访问('.$data['id'].')',"来源：{$reffer}");
		if(!isset($data))
		{
			// 如果没有这个帖子
			if(!isset($userinfo))
			{
				// 如果用户没有登录
				$this->error("您请求的页面尚未收录！","/");
			}else{
				$this->success("请求的页面尚未收录！正在跳转到收录流程。","/Post/add/id/{$id}");
			}
		}else{
			$uid = $data['uid'];
			$post->where("postid='{$id}'")->setInc('viewcount');
			$user = M("User");
			$userdata = $user->where("id={$uid}")->find();
			$pageData = array(
					'title' => $data['postname'],
					'data' => $data,
					'user' => $userdata,
					'style'=>array(C('cacheurl').'css/colorbox.css'),
					'script'=>array(C('cacheurl').'js/jquery.colorbox.min.js',C('cacheurl').'js/ZeroClipboard.min.js','//static.duoshuo.com/embed.js')
					);
			$this->assign($pageData);
			$this->display('index');
		}
	}
}