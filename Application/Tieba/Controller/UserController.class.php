<?php
namespace Tieba\Controller;
use Think\Controller;
class UserController extends Controller {
	//public function _empty($id){
	//	$this->showpage($id);
	//}
	
	public function logout()
	{
		$log = A('Log','Extend');
		$url = $_GET['url'];
		if(!isset($url))
		{
			$url = '/';
		}
		if(isset($_SESSION['userinfo']))
		{
			$data = $_SESSION['userinfo'];
			$log->log('用户'.$data['username'].'(id='.$data['id'].')'.'退出', var_export($_SERVER,true),1);
			unset($_SESSION['userinfo']);
			$this->success("已成功退出登录！",$url);
		}
	}
	
	public function oauth()
	{
		$exthis = A('User','Extend');
		$code = $_GET['code'];
		
		$url = $_GET['url'];
		if(!isset($url))
		{
			$url = C('siteurl').'/';
		}
		if(!isset($code))
		{
			$this->error("未授权的访问操作！",$url);
		}
		
		$token = $exthis->getAccessToken('code',array('code'=>$code,'redirect_uri'=>$url));
		if(0 == $token['code'])
		{
			$duid = $token['user_id'];
			$oauth = M('oauth');
			$user = M("User");
			$oauthdata = $oauth->where("duid={$duid}")->find();
			if(!isset($oauthdata))
			{
				$oauthdata = array(
					'token' => $token['access_token'],
					'expires' => $token['expires_in'],
					'remind' => $token['remind_in'],
					'duid' => $token['user_id'],
					'modifydate' => date('Y-m-d H:i:s')
				);
				if(isset($_SESSION['userinfo']))
				{
					$oauthdata['uid'] = $_SESSION['userinfo']['id'];
				}
				$oauthdata['id'] = $id = $oauth->add($oauthdata);
				if(0 >= $id)
				{
					$this->error('用户信息获取失败！',$url);
				}
			}else{
				$oauthupdata = array(
						'token' => $token['access_token'],
						'expires' => $token['expires_in'],
						'remind' => $token['remind_in'],
						'modifydate' => date('Y-m-d H:i:s')
				);
				$oauth->where('id='.$oauthdata['id'])->setField($oauthupdata);
			}
			//dump($oauthdata);
			// 判定UID是否存在
			if(isset($oauthdata['uid']))
			{
				// 存在UID则根据UID进行用户登录
				$data = $user->where('id='.$oauthdata['uid'])->find();
				$ajax = $exthis->login($data);
				if(200 == $ajax['status'])
				{
					$this->success('通行证登录成功！',$url);
					return;
				}else{
					$this->error('通行证登录失败：'.$ajax['desc'],$url);
				}
			}else{
			
				// 不存在UID则跳转到绑定界面注册一个帐号
				$key = json_encode(array('id'=>$duid,'u'=>$url));
				
				$this->redirect('/User/create',array('key'=>base64_encode($key)));
			}
		}else{
			$this->error('用户信息获取失败！'.$token['errorMessage'],$url);
		}
		
		//$info = $this->getUserInfo($token['user_id']);
		//dump($info);
		
	}
	
	public function create($key)
	{
		$params = json_decode(base64_decode($key),true);
		$duid = $params['id'];
		$url = $params['u'];
		$exthis = A('User','Extend');
		// 注册帐号页面
		// echo $duid;
		$info = $exthis->getUserInfo($duid);
		$this->assign('title','新用户注册');
		$this->assign('info',$info);
		$this->assign('url',$url);
		//dump($info);
		$this->display();
	}
	
	/*array (size=9)
  'user_id' => string '10441657' (length=8)
  'name' => string '△青龙' (length=9)
  'url' => string 'http://www.baidu.com/p/△青龙' (length=32)
  'avatar_url' => string 'http://himg.bdimg.com/sys/portrait/item/f261e296b3e99d92e9be99ea05.jpg' (length=70)
  'threads' => int 0
  'comments' => int 0
  'social_uid' => 
    array (size=1)
      'baidu' => string '1627503669' (length=10)
  'post_votes' => string '0' (length=1)
  'connected_services' => 
    array (size=1)
      'baidu' => 
        array (size=5)
          'name' => string '△青龙' (length=9)
          'avatar_url' => string 'http://himg.bdimg.com/sys/portrait/item/f261e296b3e99d92e9be99ea05.jpg' (length=70)
          'url' => string 'http://www.baidu.com/p/△青龙' (length=32)
          'description' => string '欢迎光临!' (length=13)
          'service_name' => string 'baidu' (length=5)*/
	
	//protected function showpage($id){
	//	echo 'id='.$id;
		
	//	$x = var_export($_SERVER,true);
		
	//	echo '<br />x='.$x;
	//}
}