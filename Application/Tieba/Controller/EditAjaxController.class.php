<?php
namespace Tieba\Controller;
use Think\Controller;
class EditAjaxController extends Controller {
	
	
	public function check()
	{
		$log = A('Log','Extend');
		if(!IS_POST)
		{
			$this->error('无效的地址！');
		}
		$id = $_POST['id'];
		$userinfo = $_SESSION['userinfo'];
		if(!isset($userinfo))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无效的帐号信息！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		if(!isset($id))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无效的贴子信息！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		$post = M('Post');
		$checkdata = $post->where("postid='{$id}'")->find();
		if(isset($checkdata))
		{
			$ajax = array(
					'status'=>304,
					'desc'=>'贴子信息已存在！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		
		$cutXmlStart = C('cutXmlStart');
		$cutXmlEnd = C('cutXmlEnd');
		
		$url = C('baiduPostUrl').$id;
		libxml_use_internal_errors(true);
		try{
			$file = file_get_contents($url);
			// 找到标题
			$forumname='未知来源';
			if(0 < preg_match_all("/fname=[\"'](\S[^\"']*)[\"']/",$file,$strOK))
			{
				$forumname= $strOK[1][0].'贴吧';
			}
			$xmlStart = strpos($file,$cutXmlStart);
			if($xmlStart < 0)
			{
				$ajax = array(
					'status'=>404,
					'desc'=>'无效的贴子信息！'
				);
				$this->ajaxReturn($ajax);
				return;
			}
			//$titleStart += strlen($this->cutXmlStart);
			$xmlEnd = strpos($file,$cutXmlEnd);
			$content = substr($file,$xmlStart,$xmlEnd-$xmlStart);
			$content = $this->strReplace($content);
			$file = A('File','Extend');
			$cacheFile = $file->savePostXML($id,$content);
			
			$doc = simplexml_load_string( $content );
			if(!$doc)
			{
				$log->error("xml解析失败[id={$id}]",$content );
				$errors = libxml_get_errors();
				 
				foreach ($errors as $error) {
					$errorstr = $this->display_xml_error($error, $xml);
					$log->error("xml解析错误[id={$id}]", $errorstr);
				}
				 
				libxml_clear_errors();
				$ajax = array(
					'status'=>500,
					'desc'=>'解析页面时出现错误！'
				);
				$this->ajaxReturn($ajax);
				return;
			}
			$title = trim($doc->xpath('//h1')[0]);
			$author = trim($doc->xpath('//div[@author]/@author')[0]);
			$picList = $doc->xpath('//cc//img[@class="BDE_Image"]');
			$posttype = sizeof($picList) > 5 ? -1:0;
			
			
			$data = array(
					'postid' => $id,
					'postname' => $title,
					'postauthor' => $author,
					'posttype' => $posttype,
					'posturl' => $url,
					'cachefile' => $cacheFile,
					'uid' => $userinfo['id'],
					'forumname'=> $forumname,
					'modifyuid' => $userinfo['id'],
					'modifydate' => date('Y-m-d H:i:s')
					);
			$insertId = $post->data($data)->add();
			if(!empty($insertId))
			{
				$ajax = array(
						'status'=>200,
						'desc'=>'页面分析完成。',
						'title'=>$title,
						'id'=>$insertId,
						'type'=>$posttype
				);
				$this->ajaxReturn($ajax);
				return;
			}else{
				$ajax = array(
						'status'=>500,
						'desc'=>'添加贴子数据失败！'
				);
				$this->ajaxReturn($ajax);
				return;
			}
			
			//echo $content;
			
		}catch(Exception $e)
		{
			$log->error('执行出错',$e->getTraceAsString());
			$ajax = array(
					'status'=>500,
					'desc'=>'处理请求时出现错误！'
			);
			$this->ajaxReturn($ajax);
		}
	}
	
	public function getpiclist()
	{
		$log = A('Log','Extend');
		if(!IS_POST)
		{
			$this->error('无效的地址！');
		}
		$id = $_POST['id'];
		$userinfo = $_SESSION['userinfo'];
		if(!isset($userinfo))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无效的帐号信息！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		if(!isset($id))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无效的帖子信息！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		$post = M('Post');
		$postdata = $post->where("id={$id}")->find();
		if(!isset($postdata))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无法读取帖子数据！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		if($postdata['posttype'] != -1)
		{
			$ajax = array(
					'status'=>413,
					'desc'=>'帖子已经被其他用户处理，请返回查看。'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		$content = file_get_contents($postdata['cachefile']);
		$doc = simplexml_load_string( $content );
		if(!$doc)
		{
			$log->error("xml解析失败[id={$id}]",$content );
			$errors = libxml_get_errors();
				
			foreach ($errors as $error) {
				$errorstr = $this->display_xml_error($error, $xml);
				$log->error("xml解析错误[id={$id}]", $errorstr );
			}
				
			libxml_clear_errors();
			$ajax = array(
					'status'=>500,
					'desc'=>'解析页面时出现错误！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		
		$postimg = M('Postimg');
		
		$picList = $doc->xpath('//cc//img[@class="BDE_Image"]');
		$pindex = 0;
		foreach($picList as $pic)
		{
			$picSrc = trim($pic->xpath('@src')[0]);
			//echo $picSrc;
			
			if(preg_match("/\/sign=(.*?)\/(.*?).jpg/",$picSrc,$picInfo))
			{
				//"/http:\/\/?[^\s]+/i"
				
				$picdata = array(
						'pid'=>$postdata['id'],
						'imgsign'=>$picInfo[1],
						'imgcode'=>$picInfo[2],
						'imgsrc'=>$picInfo[0],
						'uid'=>$userinfo['id'],
						'pindex'=> ($pindex+1)*10
						);
				if($postimg->add($picdata)>0)
				{
					$pindex++;
				}
			}
			//$picSign = 
			//dump();
		}
		
		if($pindex>0)
		{
			$post->where('id='.$postdata['id'])->setField(array(
					'modifydate'=>date('Y-m-d H:i:s'),
					'uid'=>$userinfo['id'],
					'posttype'=>1,
					'exdata'=>$pindex
					));
			$ajax = array(
					'status'=>200,
					'desc'=>'图片地址解析完成！',
					'count'=>$pindex
			);
			$this->ajaxReturn($ajax);
			return;
		}
		
	}
	
	public function batch($id)
	{
		$log = A('Log','Extend');
		//if(!IS_POST)
		//{
		//	$this->error('无效的地址！');
		//}
		//$id = $_POST['id'];
		$userinfo = $_SESSION['userinfo'];
		if(!isset($userinfo))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无效的帐号信息！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		if(!isset($id))
		{
			$ajax = array(
					'status'=>403,
					'desc'=>'无效的帖子信息！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		$post = M('Post');
		$checkdata = $post->where("postid='{$id}'")->find();
		if(isset($checkdata))
		{
			$ajax = array(
					'status'=>304,
					'desc'=>'帖子信息已存在！'
			);
			$this->ajaxReturn($ajax);
			return;
		}
		
		$cutXmlStart = C('cutXmlStart');
		$cutXmlEnd = C('cutXmlEnd');
		
		$url = C('baiduPostUrl').$id;
		libxml_use_internal_errors(true);
		try{
			$file = file_get_contents($url);
			// 找到标题
			$xmlStart = strpos($file,$cutXmlStart);
			if($xmlStart < 0)
			{
				$ajax = array(
					'status'=>404,
					'desc'=>'无效的帖子信息！'
				);
				$this->ajaxReturn($ajax);
				return;
			}
			//$titleStart += strlen($this->cutXmlStart);
			$xmlEnd = strpos($file,$cutXmlEnd);
			$content = substr($file,$xmlStart,$xmlEnd-$xmlStart);
			$content = $this->strReplace($content);
			
			$cacheFile = $this->savePostFile($id,$content);
			
			$doc = simplexml_load_string( $content );
			if(!$doc)
			{
				$log->error("xml解析失败[id={$id}]",$content );
				$errors = libxml_get_errors();
				 
				foreach ($errors as $error) {
					$errorstr = $this->display_xml_error($error, $xml);
					$log->error("xml解析错误[id={$id}]", $errorstr);
				}
				 
				libxml_clear_errors();
				$ajax = array(
					'status'=>500,
					'desc'=>'解析页面时出现错误！'
				);
				$this->ajaxReturn($ajax);
				return;
			}
			$title = trim($doc->xpath('//h1')[0]);
			$author = trim($doc->xpath('//div[@author]/@author')[0]);
			$ccList = $doc->xpath('//cc/div');
			
			foreach($ccList as $cc)
			{
				echo $cc->asXml();
			}
			
		}catch(Exception $e)
		{
			$log->error('执行出错',$e->getTraceAsString());
			$ajax = array(
					'status'=>500,
					'desc'=>'处理请求时出现错误！'
			);
			$this->ajaxReturn($ajax);
		}
	}
	
	private function strReplace($str)
	{
		//style="top: 0px; left:0px"
		$str = str_replace('style="top:','data-style="top:',$str);
		//class="j_icon_slot old_icon_size"
		$str = str_replace('class="j_icon_slot ','data-class="j_icon_slot ',$str);
		//<script>
		$str = str_replace('<script>','<script><![CDATA[',$str);
		$str = str_replace('</script>',']]></script>',$str);
		
		$str = preg_replace(C('tiebaRegExp'),C('tiebaRegRep'),$str);
		
		return $str;
	}
	
	
	
	private function display_xml_error($error, $xml)
	{
		switch ($error->level) {
			case LIBXML_ERR_WARNING:
				$return .= "Warning $error->code: ";
				break;
			case LIBXML_ERR_ERROR:
				$return .= "Error $error->code: ";
				break;
			case LIBXML_ERR_FATAL:
				$return .= "Fatal Error $error->code: ";
				break;
		}
	
		$return .= trim($error->message) .
		"\n  Line: $error->line" .
		"\n  Column: $error->column";
	
		if ($error->file) {
			$return .= "\n  File: $error->file";
		}
	
		return $return;
	}
}