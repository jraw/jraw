<?php
namespace Tieba\Controller;
use Think\Controller;
class PicController extends Controller {
	
	public function thumb($code){
		//If-Modified-Since
		if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE']== $dt) {
			header("HTTP/1.0 304 Not Modified");
			return;
		}
		
		$file = A('File','Extend');
		header('Content-type: image/jpeg');
		
		//缓存
		$dt =date("D, d M Y H:m:s T");
		header("Last-Modified: {$dt}");
		header("Cache-Control: max-age=844000");
		
		
		
		$lcache = $file->checkThumbExist($code);
		$url = C('thumbPicUrl').$code.'.jpg';
		$content = file_get_contents(empty($lcache)?$url:$lcache);
		
		if(empty($lcache))$file->savePostThumb($code,$content);
		echo $content;
	}
	
	public function full($post,$id){
		$reffer = $_SERVER['HTTP_REFERER'];
		if(!isset($reffer))
		{
			header("HTTP/1.0 404 Not Found");
			return;
		}
		if(0 != strpos($reffer,C('siteUrl')))
		{
			header("HTTP/1.0 404 Not Found");
			return;
		}
		//If-Modified-Since
		if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE']== $dt) {
			header("HTTP/1.0 304 Not Modified");
			return;
		}
		header('Content-type: image/jpeg');
		
		//缓存
		$dt =date("D, d M Y H:m:s T");
		header("Last-Modified: {$dt}");
		header("Cache-Control: max-age=844000");
		
		$postimg = M('Postimg');
		$imgdata = $postimg->where("id={$id}")->find();
		$postimg->where("id={$id}")->setInc('viewcount');
		$code = $imgdata['imgcode'];
		$iscache = isset($imgdata['cachepath']);
		$url = $iscache?$imgdata['cachepath']:(C('fullPicUrl').$code.'.jpg');
		
		$content = file_get_contents($url);
		
		if(!$iscache && C('cacheLimit') < $imgdata['viewcount'])
		{
			$file = A('File','Extend');
			$imgdata['cachepath'] = $file->savePostImage("{$post}-{$code}",$content);
			if(isset($imgdata['cachepath']))$postimg->save($imgdata);
		}

		echo $content;
	}
}