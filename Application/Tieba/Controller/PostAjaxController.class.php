<?php
namespace Tieba\Controller;
use Think\Controller;
class PostAjaxController extends Controller {
	public function getlist(){
		$id = $_POST['id'];
		
		$postimg = M('Postimg');
		$imgdata = $postimg->where("pid={$id}")->order('pindex')->select();
		$ajax = array(
				'status'=>200,
				'desc'=>'图片信息获取成功！',
				'count'=>sizeof($imgdata),
				'img'=>array()
			);
		foreach($imgdata as $img)
		{
			$ajax['img'][] = array(
					'id'=>$img['id'],
					'code'=>$img['imgcode']	
				);
		}
		$this->ajaxReturn($ajax);
	}
	
	public function check()
	{
		$id = $_POST['id'];
		$url = $_POST['url'];
		
		$userinfo = $_SESSION['userinfo'];
		
		$post = M('Post');
		$data = $post->where("postid='{$id}'")->find();
		
		$log = A('Log','Extend');
		$log->log("用户请求访问({$id})",$url);
		
		if(!$userinfo && !$data)
		{
			$ajax = array(
					'status'=>404,
					'desc'=>'指定的帖子尚未被本网站收录。'
			);
			$this->ajaxReturn($ajax);
		}
		else if(!$data)
		{
			$ajax = array(
					'status'=>301,
					'desc'=>'帖子尚未被收录，正在跳转到收录页面...',
					'url'=>C('siteurl')."post/add/id/{$id}"
			);
			$this->ajaxReturn($ajax);
		}else{
			$ajax = array(
					'status'=>200,
					'desc'=>'帖子已收录，正在页面跳转...',
					'url'=>C('siteurl')."p/{$id}"
			);
			$this->ajaxReturn($ajax);
		}
		
	}
}