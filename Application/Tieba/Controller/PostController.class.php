<?php
namespace Tieba\Controller;
use Think\Controller;
class PostController extends Controller {
	public function index()
	{
		$this->update(1);
	}
	
	public function page($p=1)
	{
		$this->update($p);
	}
	
	public function update($page=1)
	{
		$post = M('Post');
		$limit = C('pageLimit');
		$total = $post->count();
		$pagecount = ceil($total/$limit);
		if($page > $pagecount)
		{
			$this->error('页数无效，即将跳转到最后一页。',U('/Post/update',"page={$pagecount}"));
			return;
		}
		$data = $post->where('posttype=1')->order('modifydate desc')->page($page,$limit)->select();
		$this->assign('total',$total);
		$this->assign('list',$data);
		$this->assign('prev',$page-1);
		$this->assign('next',$page==$pagecount?0:($page+1));
		$this->assign('title',"收录列表 第 {$page} 页");
		$this->display('update');
	}
	
	public function add($id)
	{
		//echo "Edit Page(id={$id})";
		$this->assign('postid',$id);
		$this->assign('title',"帖子收录[id={$id}]");
		$this->display();
	}
	
	public function batch($id)
	{
		$this->assign('postid',$id);
		$this->assign('title',"整合帖收录[id={$id}]");
		$this->display();
	}
}