<?php
namespace Tieba\Controller;
use Think\Controller;
class UserAjaxController extends Controller {
	public function login()
	{
		if(!IS_POST)
		{
			$this->error('无效的地址！');
		}
		$user = M("User");
		$exthis = A('User','Extend');
		$userPost = array(
				'email' => trim($_POST['user']),
				'password' => md5(trim($_POST['pwd']))
		);
		$data = $user->where($userPost)->find();
		
		$this->ajaxReturn($exthis->login($data));
		
	}
	
	public function check()
	{
		$log = A('Log','Extend');
		if(!IS_POST)
		{
			$this->error('无效的地址！');
		}
		$data = $_SESSION['userinfo'];
		$log->log(' 用户'.get_client_ip().'访问页面', '用户'.get_client_ip().(isset($data)?(' - '.$data['username'].'[id='.$data['id'].'] '):' ').'访问页面地址：'.$_POST['url']."\r\n客户端：".$_SERVER['HTTP_USER_AGENT']);
		if(isset($data))
		{
			
			$ajax = array(
					'status'=>200,
					'desc'=>'登录成功！',
					'name'=>$data['username'],
					'menu'=>array(array('url'=>'/User/logout','text'=>'退出'))
					);
			$this->ajaxReturn($ajax);
		}else{
			$ajax = array(
					'status'=>404,
					'desc'=>'无效的用户数据！'
			);
			$this->ajaxReturn($ajax);
		}
	}
	
	public function checkemail()
	{
		if(!IS_POST)
		{
			$this->error('无效的地址！');
		}
		$user = M("User");
		$email = trim($_POST['email']);
		$data = $user->where("email='{$email}'")->find();
		if(isset($data))
		{
			$ajax = array(
					'status'=>200,
					'desc'=>'邮件地址已存在！'
			);
			$this->ajaxReturn($ajax);
		}else{
			$ajax = array(
					'status'=>404,
					'desc'=>'邮件地址不存在！'
			);
			$this->ajaxReturn($ajax);
		}
	}
	
	public function create()
	{
		if(!IS_POST)
		{
			$this->error('无效的地址！');
		}
		$user = M("User");
		$log = A('Log','Extend');
		$exthis = A('User','Extend');
		$email = trim($_POST['email']);
		$data = $user->where("email='{$email}'")->find();
		if(isset($data))
		{
			$this->error('邮件地址已经存在！');
			return;
		}
		
		$data = array(
			'email'=>$email,
			'username'=>$_POST['nick'],
			'avatar'=>$_POST['avatar'],
			'lastvisittime'=>date('Y-m-d H:i:s'),
			'password' => md5(trim($_POST['pwd']))
		);
		
		$data['id'] = $id = $user->add($data);
		
		if($id < 0)
		{
			$this->error('注册用户失败！');
		}
		$duid = $_POST['duid'];
		$url = $_POST['url'];
		$oauth = M('oauth');
		$oauthdata = array(
				'uid' => $id,
				'modifydate' => date('Y-m-d H:i:s')
		);
		if($oauth->where('duid='.$duid)->setField($oauthdata))
		{
			$json = $exthis->login($data);
			if(200 == $json['status'])
			{
				$this->success('通行证用户绑定成功！',$url);
			}
		}else{
			$this->error('用户绑定失败！');
		}
		/*'nick' => string '' (length=9)
  'email' => string 'dr4@163.com' (length=11)
  'pwd' => string '123456' (length=6)
  'repwd' => string '123456' (length=6)
  'agreement' => string 'on' (length=2)
  'duid' => string '10441657' (length=8)*/
		
	}
}