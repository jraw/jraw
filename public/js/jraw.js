~function(window,undefined){
	'use strict';
	
	if (typeof(jQuery) === typeof(undefined)) 
	{
		throw new Error('This site\'s JavaScript requires jQuery')
	}
	
	var $ = jQuery;
	
	if(typeof(jRawPage) === typeof(undefined))
	{
		window.jRawPage = {
			site:null,
			regex:{
	    		tieba:function(v){return (new RegExp('/(tieba\.com|tieba\.baidu\.com)/')).test(v);},
	    		page:function(v){return (new RegExp('/p/(\\d*)')).exec(v);},
	    		mo:function(v){return (new RegExp('[?|&]kz=(\\d*)')).exec(v);},
	    		email:function(v){return /^[a-zA-Z0-9_+.-]+\@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,4}$/.test(v);},
				nickname:function(v){return /^.{2,30}$/.test(v);},
				password:function(v){return /^\w{6,30}$/.test(v);}
	    	}
		};
	}
}(window,undefined);

// Layout.html
~function(window,$,_p,undefined)
{
	'use strict';
	_p['tips'] = function(o,t,s,r){
		var alert = $('<div class="alert alert-danger alert-dismissible" style="margin-top:10px;" role="alert" />').text(t).appendTo($(o));
		$('<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>').prependTo(alert);
		if(s)setTimeout(function(){$(alert).remove();},s*1000);
		if(r)alert.attr('for',r);
	};

			
	var _ = _p['layout'] = {
		loginSuccess: function(d){
			var userArea = $("<li />").addClass("dropdown").appendTo( $("#userPanel").empty() );
			//$("#userPanel .dropdown");
			$('<a href="#" data-toggle="dropdown" />').addClass("fa fa-user dropdown-toggle").text("  "+d['name']).prependTo(userArea);
			$('<ul role="menu" id="userMenu" />').addClass("dropdown-menu").appendTo(userArea);
			if(d['menu'])
			{
				for(var i in d['menu'])
				{
					var item = d['menu'][i];
					$('<li />').html('<a href="'+ item['url'] +'">'+ item['text'] +'</a>').appendTo($("#userMenu"));
				}
			}
		},
		ssoUrl:function(u){return _p.site+u+"?url="+encodeURIComponent(window.location.href)+"&t="+(new Date()).getTime()}
	};
	
	_p['ready'] = function(o){
		_p.site = o.site;
		window.duoshuoQuery = 
		{
			short_name:o.dsn,
			sso: { 
	        	login: _.ssoUrl("User/oauth"),
	        	logout: _.ssoUrl("User/logout")
	    	}
		};
		
		$.post("/UserAjax/check","url="+encodeURIComponent(window.location.href),function(data){
			if(200 === data['status'])
			{
				_.loginSuccess(data);
			}
		});
		
		$("#loginAccount").on('click',function(){$("#loginWin .loginOauth").removeClass("loginOauth").addClass("loginAccount");});
		$("#loginPanelOauth a").each(function(){
			var url = $(this).attr("href") + encodeURIComponent(_.ssoUrl("User/oauth"));
			$(this).attr("href",url);
			
		});
		$("#loginOauth").on('click',function(){$("#loginWin .loginAccount").removeClass("loginAccount").addClass("loginOauth");});
		
		$("#loginBtn").on('click',function(){
			var poststr = $("#loginForm").serialize();
			$.post("/UserAjax/login",poststr,function(data){
				if(200 === data['status'])
				{
					$('#loginWin').modal('hide');
					_.loginSuccess(data['name']);
				}else{
					_p.tips("#loginWin .modal-body",data['desc']);
				}
			});
		});
		if( typeof(undefined) != typeof(_p[o.s]))
		{
			_p[o.s].ready(o.p);
		}
	};
}(window,jQuery,jRawPage,undefined);

//index.html
~function(window,$,_p,undefined)
{
	'use strict';
	var _ = _p['index'] = {
		urlcut:function(url){
			if(!_p.regex['tieba'](url))
			{
				return false;
			}
			var pidList = _p.regex['mo'](url);
			if(!pidList)
			{
				pidList = _p.regex['page'](url);
			}
			if(!pidList) return false;
			
			return pidList[1];
		},
		ready:function(p){
    		$("#postlinkbtn").on('click',function(){
    			var url = $("#postlinkaddr").val().trim();
    			var pid = _.urlcut(url);
    			if(!pid)
    			{
    				_p.tips("#mainArea .tab-content",'您输入的URL地址不符合要求',5);
    			}else{
    				$.post("/PostAjax/check",{'id':pid,'url':url},function(data){
    					switch(data['status'])
    					{
	    					case 404:
	    						_p.tips("#mainArea .tab-content",data['desc'],5);
	    						break;
	    					case 301:
	    					case 200:
	    						window.location.replace(data['url']);
	    						break;
	    					default:
	    						_p.tips("#mainArea .tab-content",'网站故障，请稍后再试！',5);
								break;
    					}
    				});
    				
    			}
    		});
		}
	};
}(window,jQuery,jRawPage,undefined);
//UserCreate.html
~function(window,$,_p,undefined)
{
	'use strict';
	var _ = _p['UserCreate'] = {
		error:function(o,t){
   			var $o = $(o).parentsUntil('.form-group').parent();
   			$('#userCreateForm .alert[for="'+o+'"]').remove();
   			if(null == t || false == t)
   			{
   				$o.removeClass('has-error').addClass("has-success");
   			}
   			else
   			{
   				$o.removeClass('has-success').addClass('has-error');
   				_p.tips("#userCreateForm .panel-body",t,false,o);
   			}
   		},
		ready:function(p){
			$("#userPanel").hide();
	   		
			var r = _p.regex;
			
	   		$("#userCreateEmail").on('change',function(){
	   			if(!r['email']($(this).val()))
	   			{
	   				_.error("#userCreateEmail","电子邮件格式不符合要求。");
	   				return;
	   			}
	   			
	   			$.post("/UserAjax/checkemail","email="+$(this).val(),function(data){
	   				switch(data['status'])
	   				{
	   				case 200:
	   					//$('#loginWin').modal('hide');
	   					_.error("#userCreateEmail",data['desc']);
	   					break;
	   				case 404:
	   					_.error("#userCreateEmail",false);
	   					break;
	   				default:
	   					_.error("#userCreateEmail",data['desc']);
						break;
	   				}
	   			});
	   		});
	   		
	   		$("#userCreateNick").on('change',function(){
	   			if(!r['nickname']($(this).val()))
	   			{
	   				_.error("#userCreateNick","昵称长度限制为2到30个字符。");
	   			}else{
	   				_.error("#userCreateNick",false);
	   			}
	   		});
	   			
	   		$("#userCreatePwd").on('change',function(){
	   	   		if(!r['password']($(this).val()))
	   	   		{
	   	   			_.error("#userCreatePwd","密码长度限制为6到30个字符。");
	   	   		}else{
	   				_.error("#userCreatePwd",false);
	   			}
	   	   	});
	   	   	
	   	   	$("#userCreateRepwd").on('change',function(){
	   	   		if($(this).val() != $("#userCreatePwd").val())
	   	   		{
	   	   			_.error("#userCreateRepwd","两次输入的密码不相同。");
	   	   		}else{
	   				_.error("#userCreateRepwd",false);
	   			}
	   	   	});
	   	   	
	   	 	$("#userCreateAgreement").on('change',function(){
		   		if(!$("#userCreateAgreement").is(":checked"))
		   		{
		   			_.error("#userCreateAgreement","必须同意用户协议才能注册。");
		   		}else{
					_.error("#userCreateAgreement",false);
				}
		   	});
	   		
	   		$("#userCreateForm").submit(function(){
	   			$('.form-group input').trigger('change');
	   			if($('#userCreateForm').find('.has-error').length == 0)
	   			{
	   				return true;
	   			}
	   			return false;
	   		});
		}
	};
}(window,jQuery,jRawPage,undefined);

//PostAdd.html
~function(window,$,_p,undefined)
{
	'use strict';
	var _ = _p['PostAdd'] = {
		id:null,
		getPicList:function(pid){
			$.post("/EditAjax/getpiclist","id="+pid,function(data){
    			if(200 === data['status'])
				{
    				$("<li />").addClass("text-success").text(data['desc']).prependTo("#PostAddList");
    				$("<li />").addClass("text-success").text('帖子内图片数：'+data['count']).prependTo("#PostAddList");
    				$("<li />").addClass("text-success").text('收录完成，请点击下方“查看页面”返回查看。').prependTo("#PostAddList");
    				_.showNextStep();
				}else{
					$("<li />").addClass("text-danger").text(data['desc']).prependTo("#PostAddList");
				}
			});
    	},
    	showNextStep:function(){
			$("#PostAddNext").removeClass('hidden');
			$("#PostAddShow").on("click",function(){
				window.location.replace('/p/'+_.id+'?from=postadd');
			});
			$("#PostAddEdit").on("click",function(){
				window.location.replace('/Post/edit/id/'+_.id+'?from=postadd');
			});
		},
		ready:function(p){
			_.id = $("#PostAddArea").data("id");
    		var url = p.tb+_.id;
    		
    		$("<li />").addClass("text-success").text('访问URL：'+url).prependTo("#PostAddList");
    		
    		$.post("/EditAjax/check","id="+_.id,function(data){
    			if(200 === data['status'])
				{
    				$("<li />").addClass("text-success").text(data['desc']).prependTo("#PostAddList");
    				$("<li />").addClass("text-success").text('帖子标题为：'+data['title']).prependTo("#PostAddList");
    				switch(data['type'])
    				{
    				case -1:
    					$("<li />").addClass("text-success").text('正在导入图片信息...').prependTo("#PostAddList");
    					_.getPicList(data['id']);
    					break;
    				case 0:
    					$("<li />").addClass("text-danger").text('帖子格式不对，如果是整合帖请点击下方说明中的链接切换。').prependTo("#PostAddList");
    					break;
    				default:
    					$("<li />").addClass("text-danger").text('帖子已经被其他用户处理，请返回查看。').prependTo("#PostAddList");
    					break;
    				}
				}else{
					$("<li />").addClass("text-danger").text(data['desc']).prependTo("#PostAddList");
				}
    		});
		}
	};
}(window,jQuery,jRawPage,undefined);

//PostView.html
~function(window,$,_p,undefined)
{
	'use strict';
	var _ = _p['PostView'] = {
		thumbUrl:function(img){return '/Pic/thumb/code/'+img['code']+'?pic=.jpg';},
		fullUrl:function(img,pid){return '/Pic/full/post/'+pid+'/id/'+img['id']+'?pic=.jpg';},
		commentList:function(c)
		{
			var el = document.createElement('div');
		    el.setAttribute('data-thread-key', 'post'+pid);
		    el.setAttribute('data-url', window.location.href);
		    DUOSHUO.EmbedThread(el);
		    jQuery(c).append(el);
		},
		initDownWin:function(img){
			var url = $("#postDownLink").data('url');
			var client = new ZeroClipboard( $("#postDownBtn") );
			var text = "";
			for(var i in img)
			{
				var code = img[i]['code'];
				text += url+code+'.jpg\r\n';
			}
			$("#postDownLink").val(text);
		},
		ready:function(p){
			var pid = $("#picArea").data('id');
			$.post("/PostAjax/getlist","id="+pid,function(data){
				if(200 === data['status'])
				{
					//console.log(data);
					for(var i in data['img'])
					{
						var img = data['img'][i];
						var d = $("<div />").addClass('col-xs-4 col-md-2').appendTo("#picArea .row");
						var a = $("<a />").attr('href',_.fullUrl(img,pid)).addClass('thumbnail').appendTo(d);
						$("<img />").attr({'src':_.thumbUrl(img),'alt':'Page '+i}).appendTo(a);
					}
					_.initDownWin(data['img']);
					$("#picArea a").colorbox({rel:"mainPicGroup",width:"100%",height:"100%",scalePhotos:false,
						onOpen:function(){
							$('html').css("overflow","hidden");
						},
						onClosed:function(){
							$('html').css("overflow","auto");
						}
					});
					
				}
			});
			
			$("#picAreaExpand").on('click',function(){
				$("#picArea").removeClass('picAreaClose');
				$(this).addClass('hidden');
				$("#picAreaClose").removeClass('hidden');
			});
			$("#picAreaClose").on('click',function(){
				$("#picArea").addClass('picAreaClose');
				$(this).addClass('hidden');
				$("#picAreaExpand").removeClass('hidden');
			});
		}
	};
}(window,jQuery,jRawPage,undefined);
