-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015-01-26 01:31:20
-- 服务器版本: 5.5.41-0ubuntu0.14.04.1
-- PHP 版本: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `jraw_cache`
--
CREATE DATABASE IF NOT EXISTS `jraw_cache` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `jraw_cache`;

-- --------------------------------------------------------

--
-- 表的结构 `admingroup`
--

CREATE TABLE IF NOT EXISTS `admingroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `authgroup` varchar(100) NOT NULL COMMENT '权限组',
  `desc` text NOT NULL COMMENT '说明',
  `byuid` int(11) NOT NULL COMMENT '添加者',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录时间',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '日志类别',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `desc` varchar(200) DEFAULT NULL COMMENT '日志简述',
  `log` text COMMENT '日志详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=174 ;

-- --------------------------------------------------------

--
-- 表的结构 `oauth`
--

CREATE TABLE IF NOT EXISTS `oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `token` varchar(50) NOT NULL COMMENT '授权码',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `expires` int(11) NOT NULL COMMENT '过期时间',
  `remind` int(11) NOT NULL COMMENT '提示时间',
  `duid` int(11) NOT NULL COMMENT '多说用户ID',
  `uid` int(11) DEFAULT NULL COMMENT '本地用户ID',
  `userdata` text COMMENT '用户数据',
  `modifydate` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `postid` varchar(100) NOT NULL COMMENT '帖子ID',
  `postname` varchar(200) NOT NULL COMMENT '帖子名称',
  `postauthor` varchar(100) NOT NULL COMMENT '帖子作者',
  `posttype` int(11) NOT NULL DEFAULT '0' COMMENT '帖子类型',
  `posturl` varchar(200) NOT NULL,
  `cachefile` varchar(300) NOT NULL,
  `exdata` int(11) NOT NULL COMMENT '附加数据',
  `forumname` varchar(300) DEFAULT NULL COMMENT '贴吧名称',
  `uid` int(11) NOT NULL COMMENT '提交者ID',
  `uploaddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '提交时间',
  `modifyuid` int(11) NOT NULL COMMENT '修改者ID',
  `modifydate` datetime NOT NULL COMMENT '修改时间',
  `viewcount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- 表的结构 `postimg`
--

CREATE TABLE IF NOT EXISTS `postimg` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '贴图ID',
  `pid` int(11) NOT NULL COMMENT '帖子ID',
  `imgsign` varchar(100) NOT NULL COMMENT '图片Sign',
  `imgcode` varchar(100) NOT NULL COMMENT '图片Code',
  `imgsrc` varchar(300) NOT NULL COMMENT '图片源链接',
  `uid` int(11) NOT NULL COMMENT '录入者',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '录入时间',
  `pindex` int(11) NOT NULL COMMENT '排序',
  `viewcount` int(11) NOT NULL DEFAULT '0',
  `cachepath` varchar(200) DEFAULT NULL,
  `thumbpath` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=284 ;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `avatar` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisittime` datetime NOT NULL,
  `scope` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
