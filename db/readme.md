# 数据库格式说明 #

### user ###


* `id` int(11) 用户ID
* `username` varchar(100) 昵称，可以修改
* `email` varchar(100) 邮件地址，唯一
* `avatar` varchar(300) 头像地址
* `password` varchar(100) 密码
* `createtime` timestamp 创建时间
* `lastvisittime` datetime 最后访问时间
* `scope` int(11) 全局权限

### admingroup ###
分配管理员所在的权限组信息。每个管理员可以归属多个权限组，每个权限组仅有少数操作权限。可以相对自由的分配权限信息。

* `id` int(11) 主键
* `uid` int(11) 用户ID
* `authgroup` varchar(100) 权限组
* `desc` text 备注信息
* `byuid` int(11) 添加者
* `createdate` timestamp 添加时间

### log ###
日志表，记录一些基础的错误日志等信息。

* `id` int(11) 日志ID，主键
* `time` timestamp 记录时间
* `type` int(11) 日志类别：
> * 0:访问日志
> * 1:登录日志
> * -1:调试日志
> * 99:错误日志
> * 999:严重错误日志
* `uid` int(11) 操作用户的ID
* `desc` varchar(200) 日志简述
* `log` text 日志详情

### oauth ###
社交帐号通行证登录表（多说），可能会有变更

* `id` int(11) 主键
* `token` varchar(50) 授权码
* `createdate` timestamp 创建时间
* `expires` int(11) 过期时间（秒）
* `remind` int(11) 提示时间（秒）
* `duid` int(11) 多说用户ID
* `uid` int(11) 本地用户ID
* `userdata` text 用户数据
* `modifydate` datetime 更新时间

### post ###
百度贴吧收录主表，记录贴子相关的信息以供回溯。

* `id` int(11) 主键，本站贴子ID
* `postid` varchar(100) 贴吧贴子ID
* `postname` varchar(200) 贴子标题
* `postauthor` varchar(100) 帖子作者
* `posttype` int(11) 帖子类型
> * -1:隐藏的，未处理的
> * 0:整合贴，非漫画贴子
> * 1:漫画贴
* `posturl` varchar(200) 原帖地址
* `cachefile` varchar(300) 缓存XML文件地址
* `exdata` int(11) 附加数据
> 如果贴子是漫画贴，对应值为图片数。
>
> 如果贴子是整合帖，对应的值为关联的漫画合集信息。
* `forumname` varchar(300) 来源贴吧名称
* `uid` int(11) 提交者ID
* `uploaddate` timestamp 提交时间
* `modifyuid` int(11) 修改者ID
* `modifydate` datetime 修改时间
* `viewcount` int(11) 点击数

### postimg ###
百度贴吧收录图片信息表，记录每个贴子中获得的图片信息。

* `id` int(11) 主键，贴图ID
* `pid` int(11) 本站贴子ID
* `imgsign` varchar(100) 图片Sign
* `imgcode` varchar(100) 图片Code
* `imgsrc` varchar(300) 图片源链接
* `uid` int(11) 提交者ID
* `createdate` timestamp 提交时间
* `pindex` int(11) 排序
* `viewcount` int(11) 点击数
* `cachepath` varchar(200) 完整图片缓存地址
* `thumbpath` varchar(300) 预览图缓存地址
