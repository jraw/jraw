# [JRaw.net](http://tieba.JRaw.net) Project #

This project is a part of Project.Scanlation.Info.  It collect raw manga chapters that someone shared in a Chinese forum named Baidu Tieba.
这个项目是Project.Sacnlation.Info的一个部分。创建目的是收集来源于中文论坛百度贴吧上用户分享的漫画生肉。

### 目前使用到的开源项目 ###

* [ThinkPHP](http://thinkphp.cn/)
* [jQuery](http://jQuery.com/)
* [Bootstrap](http://getbootstrap.com/)
* [FontAwesome](http://FontAwesome.io/)
* [jquery.colorbox](http://www.jacklmoore.com/colorbox/)
* [ZeroClipboard](http://zeroclipboard.org/)

### 安装方法 ###

1. 将`db`目录下的数据库导入您的MySQL
2. 将`Application/Common/Conf_bak`重命名为`Application/Common/Conf`，修改`config.php`中有关数据库的内容。
3. 将`Application/Tieba/Conf_bak`重命名为`Application/Tieba/Conf`，修改`config.php`中提示的几处地址信息和密钥的取值。
4.将除了`db`文件夹以外的所有内容上传到网站根路径即可。
5.如果服务器有比较严格的权限分配，请在根路径上新建`cache`文件夹并且赋予`0777`的用户权限，以便程序写入相关的缓存。
### 版本记录 ###

* Alpha Version 0.0.2
> 新增最新贴子列表
>
> 增加了图片的缓存头，避免重复拉取
>
> 将页面脚本和样式信息整理到单独的文件中。
>
> 改用正则替换处理贴吧源文件中的异常格式。
>
> 修改了部分页面的布局。

* Alpha Version 0.0.1
> 基本实现了贴吧贴图帖的收录，提供在线试看。

### 如何联系我们 ###

* 登录托管本项目的Bitbucket网站，在“问题（Issues）”中留言，基本上都会回复。
* 加入本站QQ群：3005 26 266